import java.util.Scanner;
class tugaspbo
{
    public static void main(String args[])
    {
        char[] a = {'a', 'b', 'c', 'd', 'e'};
        int[][] x = {{1, 3}, {5, 2}, {0, 8}};

        Scanner input = new Scanner(System.in);
        System.out.print("Nilai : ");
        int b = input.nextInt();                //Input
        // System.out.println("Nilainya : " + b);  //output

        if (b % 2 == 0) {
            System.out.println("adalah Bilangan Genap");
        } else {
            System.out.println("adalah Bilangan Ganjil");
        }

        System.out.println("Array 1 Dimensi");
        for (int i = 0; i < 5; i++) {        //perulangan For & array 1D
            System.out.println("index " + i + " = " + a[i]);
        }
        System.out.println();

        System.out.println("Array 2 Dimensi");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                System.out.print(x[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();

        System.out.println("PERULANGAN");
        int f = 0;
        while (f < 4) { //While
            System.out.println("Hello Java");
            f++;
        }
        System.out.println();

        int e = 0;
        do { //Do while
            System.out.println("Selamat Datang di Java");
            e++;
        }
        while (e < 4);
    }
}