#include<iostream>
using namespace std;

int main()
{
    char a[3]={'a','b','c'};
	int b;
    int x[3][2] = {{1,3}, {5,2}, {0,8}};
	
	cout<<"Nilai : ";
	cin>>b;												// Input
	if (b%2==0)
	{													// If
		cout<<b<<" adalah Bilangan Genap"<<endl;
    }
	else
	{
        cout<<b<<" adalah Bilangan Ganjil"<<endl;
    }
    cout<<endl;
    
    cout<<"Array 1 Dimensi"<<endl;
    for(int i=0; i<5; i++)
	{													// Perulangan For dan Array 1 Dimensi
        cout<<"index "<<i<<" = "<<a[i]<<endl;			// Output
    }
    cout<<endl;
    
    cout<<"Array 2 Dimensi"<<endl;						// Array 2 Dimensi atau Multidimensi
    for(int i=0;i<3;i++)
	{
		for(int j=0;j<2;j++)
		{
			cout<<x[i][j]<<"\t";
		}
		cout<<endl;
	}
	cout<<endl;
    
    cout<<"PERULANGAN"<<endl;
    cout<<endl;
    int d=0;
    while (d<4)
	{										//While
        cout<<"Hello World"<<endl;
        d++;
    }
    cout<<endl;
    
	int e=0;
    do
	{												//Do while
        cout<<"C++ dan Java"<<endl;
    	e++;
    }
    while (e<4);
}

